package com.upload;

import java.net.URLConnection;
import java.nio.charset.Charset;

/**
 * @author Sergey Koptev
 * @version 1.0.0, 10.05.18
 * @since 1.0.0
 */
public class urlCharset {

    public static Charset getCharset(URLConnection conn) {

        String contentType = conn.getHeaderField("Content-Type");
        if (contentType != null) {
            if (contentType.contains("charset=")) {
                String contentTypeCharset[] = contentType.substring(contentType.lastIndexOf("charset=")).split("=");
                if (contentTypeCharset.length == 2)
                    return Charset.forName(contentTypeCharset[1]);
            }
        }
        return null;
    }
}
