package com.upload;

import java.util.Date;

/**
 * @author Sergey Koptev
 * @version 1.0.0, 10.05.18
 * @since 1.0.0
 */
public class uploadSpeed {

    long size = 0;
    long start;
    long worktime;
    long speed;

    public boolean Sleep(long size) {

        this.size = this.size + size;

        worktime = (new Date().getTime() - start);
        if (worktime > 0) {
            if (this.size / worktime > speed) {
                start = new Date().getTime();
                this.size = 0;
                return true;
            }
        }
        return false;
    }
}
