package com.upload;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.concurrent.Semaphore;

/**
 * upload URL
 *
 * @author Sergey Koptev
 * @version 1.0.0, 08.05.18
 * @since 1.0.0
 */
public class uploadUrl extends Thread {

    // семафор, ограничиваюший количество потоков загрузки URL
    Semaphore semaphore;
    // upload URL
    URL url;
    // Timeout open URL
    int timeout;
    //
    uploadSpeed speed;

    uploadUrl(Semaphore semaphore, URL url, Integer timeout, uploadSpeed speed) {

        this.semaphore = semaphore;
        this.url = url;
        this.timeout = Integer.parseInt(timeout.toString());
        this.speed = speed;
    }

    public void run() {

        try {
            semaphore.acquire();
            System.out.println(this.getId() + ":" + this.getName() + " -> " + this.getState() + " prior: " + this.getPriority());
            URLConnection conn;
            try {
                conn = url.openConnection();
                conn.setConnectTimeout(timeout);

                Charset charset;

                if ((charset = urlCharset.getCharset(conn)) == null)
                    charset = StandardCharsets.UTF_8;

                BufferedReader bufferedReaderUrl = new BufferedReader(new InputStreamReader(conn.getInputStream(), charset));
                String line;
                speed.start = new Date().getTime();
                while ((line = bufferedReaderUrl.readLine()) != null) {

                    //System.out.println(line);
                    //дырявое ведро
                    //Понизить приоритет thread и сделать задержку 300 милисекунд
                    if (speed.Sleep((long) line.length())) {

                        System.out.println("--Speed: " + this.getId() + ":" + this.getName() + " -> " + this.getState() + " prior: " + this.getPriority());
                        this.setPriority(Thread.MIN_PRIORITY);
                        Thread.sleep(300);
                    }
                    else
                        this.setPriority(Thread.NORM_PRIORITY);
                }
                bufferedReaderUrl.close();
                System.out.println(this.getId() + ":" + this.getName() + " -> " + this.getState() + " prior: " + this.getPriority() + " " + url + " uploaded...");

            } catch (IOException e) {
                System.out.println(this.getId() + ":" + this.getName() + " -> " + this.getState() + " prior: " + this.getPriority() + " " + url + " skipped...");
            }
            //sleep(600);
            semaphore.release();

        } catch (InterruptedException e) {
            System.out.println(this.getId() + ":" + this.getName() + " -> " + this.getState() + " prior: " + this.getPriority() + " " + url + " skipped...");
        }
    }
}
