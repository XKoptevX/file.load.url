package com.upload;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class Main {

    final static String usage = "Usage: fileLoadUrl -threads thread_number -speed speed_number -timeout ms -file file_name";
    final static String fileNotFound = "File not found...";
    final static String fileNameReq = "File name requirements...";

    public static void main(String[] args) {

        /**
         * Проверка количества аргуементов к оменданой строке
         */
        switch (args.length) {

            case 8:
                break;

            default:
                System.out.println(usage);
                return;
        }

        /**
         * Формируем список параметров
         */
        Map<String, Integer> params;
        params = new LinkedHashMap<>();
        /**
         * Имя входного файла со списком URL
         */
        String inputFile = null;
        try {

            int i = 0;
            while (i < args.length) {

                if (args[i].startsWith("-", 0)) {
                    switch (args[i]) {
                        case "-threads":
                            params.put(args[i].substring(1), Integer.valueOf(args[++i]));
                            break;
                        case "-speed":
                            params.put(args[i].substring(1), Integer.valueOf(args[++i]));
                            break;
                        case "-timeout":
                            params.put(args[i].substring(1), Integer.valueOf(args[++i]));
                            break;
                        case "-file":
                            inputFile = args[++i];
                            break;
                        default:
                            break;
                    }
                }
                i++;
            }
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage() + '\n' + usage);
            return;
        }
        if (params.size() != 3) {
            System.out.println(usage);
            return;
        }

        /**
         * Проверка имени файла и его существования
         */
        if (inputFile != null) {
            File file = new File(inputFile);
            if (!file.exists()) {
                System.out.println(fileNotFound + " [" + inputFile + "]");
            }
        } else {
            System.out.println(fileNameReq + '\n' + usage);
            return;
        }

        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile), StandardCharsets.UTF_8));
        } catch (FileNotFoundException e1) {
            System.out.println(e1.getLocalizedMessage());
            return;
        }
        Collection<URL> collectionUrl = new LinkedHashSet<>();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {

                if (!line.isEmpty()) {
                    URL url;
                    try {
                        url = new URL(line);
                        collectionUrl.add(url);
                    } catch (MalformedURLException e1) {
                        System.out.println(line + " skipped...");
                    }
                }
            }
            bufferedReader.close();

        } catch (IOException e1) {
            System.out.println(e1.getLocalizedMessage() + '\n' + usage);
        }

        Semaphore semaphore = new Semaphore(params.get("threads"));

        uploadSpeed speed = new uploadSpeed();
        speed.speed = params.get("speed");

        for (URL url : collectionUrl) {
            new uploadUrl(semaphore, url, params.get("timeout"), speed).start();
        }
    }
}
